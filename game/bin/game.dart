import 'dart:io';
import 'package:game/Hero.dart';
import 'package:game/Living.dart';
import 'package:game/Monster.dart';
import 'package:game/Question.dart';

class Game {
  Hero hero = Hero();
  Monster monster = Monster();
  Living liv = Living();
  int level = 0;
  List arrQuestion = [];
  int h = 0;
  int countH = 0;
  int countM = 0;

  void showWelcome() {
    print(
        '#       #       #   # # # #   #         # # # #   # # # #     #    #     # # # #');
    print(
        ' #     # #     #    #         #         #         #     #    #  # #  #   #');
    print(
        '  #   #   #   #     # # # #   #         #         #     #   #    #    #  # # # #');
    print(
        '   # #     # #      #         #         #         #     #   #    #    #  #');
    print(
        '    #       #       # # # #   # # # #   # # # #   # # # #   #         #  # # # #');
  }

  int getLevel() {
    return level;
  }

  setLevel() {
    print("You can choose level for problem :");
    // int num = int.parse(stdin.readLineSync()!);
    Question chooes = Question();
    print("Qestion");
    print(chooes.toString());
    print("Answer :");
    int ans = int.parse(stdin.readLineSync()!);
    print("Right Answer : ");
    print(chooes.getResult());

    if (ans == chooes.getResult()) {
      print("Yes");
    } else {
      print("No");
    }
  }

  void addPlayer() {
    print("Please choose your hero :");
    print("1.Hanzo 2.Zarya 3.Genji");
    int num = int.parse(stdin.readLineSync()!);
    hero.setSkill(num);
  }

  void addMonster() {
    print("Please choose monster :");
    print("1.The Doctor 2.The Nurse");
    int num = int.parse(stdin.readLineSync()!);
    monster.showSkill(num);
  }

  bool isWin() {
    return false;
  }

  void showWin() {}
  void showHeroScore() {
    print('${hero.getName()} HP : ${hero.getLife()}');
    print('ATK : ${hero.getDamage()}');
    print('Status : ${hero.getStatus()}');
    print('\n');
    // print('Right!!!');
    // if(countH <= 1){
    //   hero.
    // }
  }

  void showMonsterScore() {
    print('Monster : ${monster.getName()} \n HP : ${monster.getLife()}');
    print('ATK : ${monster.getDamage()}');
    print('Status : ${monster.getStatus()}');
  }

  void showStartGame() {
    print('---------------- Welcome to Question -----------------');
    print('Please answer the question correctly');
    Question question = Question();
    print(question.toString());
    print("Your Answer :");
    int ans = int.parse(stdin.readLineSync()!);
    if (ans == question.getResult()) {
      showHeroScore();
    } else {
      showMonsterScore();
    }
  }

  void showAllScore() {}
}

void main(List<String> args) {
  // print("Welcome to Game");
  // print("Test");
  Game game = new Game();
  game.showWelcome();
  game.addPlayer();
  game.addMonster();
  // game.setLevel();
  game.showStartGame();
  // game.showAllScore();
  game.showHeroScore();
  game.showMonsterScore();
}
