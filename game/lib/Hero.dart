// ignore: file_names
import 'package:game/Living.dart';

class Hero extends Living {
  getskill() {
    return name;
  }

  void setSkill(int skill) {
    switch (skill) {
      case 1:
        name = "Hanzo";
        life = 15;
        damage = 2;
        print('|---------------------------------|');
        print('| Your hero : $name               |');
        print('| Living : $life                     |');
        print('| Damage : $damage                      |');
        print('| Status : $status                  |');
        print('|---------------------------------|\n');
        break;
      case 2:
        name = "Zarya";
        life = 20;
        damage = 4;
        print('|---------------------------------|');
        print('| Your hero : $name               |');
        print('| Living : $life                     |');
        print('| Damage : $damage                      |');
        print('| Status : $status                  |');
        print('|---------------------------------|\n');
        break;
      case 3:
        name = "Genji";
        life = 30;
        damage = 5;
        print('|---------------------------------|');
        print('| Your hero : $name               |');
        print('| Living : $life                     |');
        print('| Damage : $damage                      |');
        print('| Status : $status                  |');
        print('|---------------------------------|\n');
        break;
      default:
    }
  }
}
