// ignore: file_names
import 'package:game/Living.dart';

class Monster extends Living {
  String weapon = '';
  String type = '';

  void showSkill(int num) {
    switch (num) {
      case 1:
        {
          name = "The Doctor";
          life = 15;
          damage = 2;
          weapon = 'Knife';
          print('|---------------------------------|');
          print('| Monster: $name             |');
          print('| Living : $life                     |');
          print('| Damage : $damage                      |');
          print('| Weapon : $weapon                  |');
          print('| Status : $status                  |');
          print('|---------------------------------|\n');
        }
        break;
      case 2:
        {
          name = "The Nurse";
          life = 20;
          damage = 5;
          weapon = 'Gun';
          print('|---------------------------------|');
          print('| Monster: $name              |');
          print('| Living : $life                     |');
          print('| Damage : $damage                      |');
          print('| Weapon : $weapon                    |');
          print('| Status : $status                  |');
          print('|---------------------------------|\n');
        }
        break;
    }
  }

  getEffect(int z) {}
  getType(String str) {}
}
