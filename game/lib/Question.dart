// ignore: file_names
import 'dart:math';

class Question {
  List<int> numbers = [Random().nextInt(20), Random().nextInt(20)];
  String? operation;

  int? answer;

  Question() {
    final List<String> operations = ['+', '-', '*'];
    int? ans;
    List<int> nums = numbers;
    // generate a random operation
    int opIdx = Random().nextInt(4);
    switch (opIdx) {
      case 0:
        ans = nums[0] + nums[1];
        break;
      case 1:
        ans = nums[0] - nums[1];
        break;
      case 2:
        ans = nums[0] * nums[1];
        break;
      case 3:
        ans = nums[0] ~/ nums[1];
        break;
    }
    operation = operations[opIdx];
    answer = ans;
  }

  getNumber() {
    return numbers;
  }

  void setNumber() {}

  getOperand() {
    return operation;
  }

  void setOperand() {}

  getResult() {
    return answer;
  }

  String getStatus() {
    return '';
  }

  @override
  String toString() {
    // ignore: unnecessary_brace_in_string_interps
    return '${numbers[0]} ${operation} ${numbers[1]} = ?';
  }
}
