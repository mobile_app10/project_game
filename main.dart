import 'dart:ffi';
import 'dart:io';

class Game {
  Hero hero = new Hero();
  Monster monster = new Monster();
  int level = 0;
  List arrQuestion = [];

  int getLevel() {
    return level;
  }

  void showWelcome() {
    print("Welcome to Game");
  }

  void setLevel() {
    print("You can choose level for problem :");
    print("1.Easy 2.Medium 3.Hard");
    int num = int.parse(stdin.readLineSync()!);

    switch (num) {
      case 1:
        {
          print("Qestion 1");
        }
        break;
      case 2:
        {
          print("Qestion 2");
        }
        break;
      case 3:
        {
          print("Qestion 3");
        }
        break;
    }
  }

  void addPlayer() {
    print("Please choose your hero :");
    print("1.Hanzo 2.Zarya 3.Genji");
    int num = int.parse(stdin.readLineSync()!);

    switch (num) {
      case 1:
        {
          print("Hero 1");
        }
        break;
      case 2:
        {
          print("Hero 2");
        }
        break;
      case 3:
        {
          print("Hero 3");
        }
        break;
    }
  }

  void addMonster() {
    print("Please choose monster :");
    print("1.The Doctor 2.The Nurse 3.The Huntress");
    int num = int.parse(stdin.readLineSync()!);

    switch (num) {
      case 1:
        {
          print("The Doctor");
        }
        break;
      case 2:
        {
          print("The Nurse");
        }
        break;
      case 3:
        {
          print("The Huntress");
        }
        break;
    }
  }

  bool isWin() {
    return false;
  }

  void showWin() {}
  void showHero() {}
  void showMonster() {}
}

class Question {
  List operaters = ['+', '-', '*', '/']; //for random operater
  List operand01 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]; //for random operand x
  List operand02 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]; //for random operand y
  List operater = []; //For keep Question from user
  List operand = []; //For keep Result from user
  double result = 0.0;
  String status = "";

  List getOperater() {
    return operater;
  }

  void setOperater() {}

  List getOperand() {
    return operand;
  }

  void setOperand() {}
  double getResult() {
    return result;
  }

  String getStatus() {
    return status;
  }

  @override
  String toString() {
    return super.toString();
  }
}

class Living {
  int life = 0;
  int damage = 0;
}

class Hero extends Living {
  String skill = "เมื่อคุณตอบคำถามถูดติดต่อกัน คุณจะสามารถเพิ่มดาเมจ 10xp";

  getskill() {}
}

class Monster extends Living {
  int effect = 0;
  String color = "Blue";
  String type = "Water";

  getEffect(int z) {}
  getType(String str) {}
}

void main(List<String> args) {
  Game game = new Game();
  print("Welcome to Game");
  game.addPlayer();
  game.addMonster();
  game.setLevel();
}
